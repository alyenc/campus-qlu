
Page({
  data: {
    buslines: [],
    realTime: [],
    startStationName: "",
    endStationName: "",
    operationTime: "",
    ticketPrice: "",
    lineName: ""
  },
  onLoad: function (options) {
    var that = this;
    wx.showToast({
      title: '加载中...',
      icon: 'loading',
      duration: 10000
    });
    wx.request({
      url: 'https://campus.alyenc.com/getBusStation.json',
      data: {
        busline: options.lineid
      },
      method: "POST",
      success: function (res) {
        that.setData({
          buslines: res.data.value.busStationDetailList,
          startStationName: res.data.value.startStationName,
          endStationName: res.data.value.endStationName,
          operationTime: res.data.value.operationTime,
          ticketPrice: res.data.value.ticketPrice,
          lineName: res.data.value.lineName
        })
        wx.hideToast();
      },
      fail: function (err) { },
      complete: function () { }
    })
  },
  realTimeBus() {
    wx.request({
      url: 'https://campus.alyenc.com/getRealTimeBus.json',
      data: {
        busline: options.lineid
      },
      method: "POST",
      success: function (res) {
        that.setData({
          realTime: res.data.value.realTimeBusDetailList
        })
        wx.hideToast();
      },
      fail: function (err) { },
      complete: function () { }
    })
  }
})

