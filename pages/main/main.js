//main.js
//获取应用实例
const app = getApp()

Page({
  data: {
    name: '齐鲁工业大学微校园',
    logoUrl: '/static/logo.png',
    
    section1: '齐鲁工大',
    section2: '校园服务',
    section3: '社会服务',
    section4: '其他服务',
    
    section1_title1: '工大新闻',
    section1_title2: '通知公告',
    section1_title3: '工大简介',
    
    section2_title1: '四六级',
    section2_title2: '考试成绩',
    section2_title3: '课表查询',
    section2_title4: '工大地图',
    section2_title5: '社团活动',
    
    section3_title1: '快递查询',
    section3_title2: '公交查询',

    section4_title1: '跳骚市场',
    section4_title2: '表白墙',
    section4_title3: '话题墙'
  },
  bindViewTap: function (event) {
    if (event.currentTarget.dataset.id === '13') {
      wx.navigateTo({
        url: '../introduce/introduce'
      })
    }
    if (event.currentTarget.dataset.id === '12') {
      wx.navigateTo({
        url: '../notice/notice'
      })
    }
    if (event.currentTarget.dataset.id === '21') {
      wx.navigateTo({
        url: '../cet/cet'
      })
    }
    if (event.currentTarget.dataset.id === '22') {
      wx.navigateTo({
        url: '../login/login?flag=grade'
      })
    }
    if (event.currentTarget.dataset.id === '23') {
      wx.navigateTo({
        url: '../login/login?flag=course'
      })
    }
    if (event.currentTarget.dataset.id === '31'){
      wx.navigateTo({
        url: '../express/express'
      })
    }
    if (event.currentTarget.dataset.id === '32') {
      wx.switchTab({
        url: '../bus/bus'
      })
    }
    if (event.currentTarget.dataset.id === '24') {
      wx.navigateTo({
        url: '../map/map'
      })
    }
    if (event.currentTarget.dataset.id === '11') {
      wx.navigateTo({
        url: '../news/news'
      })
    }
  }
})