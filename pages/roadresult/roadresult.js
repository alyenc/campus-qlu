// pages/roadresult/roadresult.js
var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var qqmapsdk;
var key = "PKDBZ-VF63P-SZCDN-LIJOR-2H6WE-BUFBR";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    starttitle: "",
    endtitle: "",
    startlng: 0,
    startlat: 0,
    endlng: 0,
    endlat: 0,
    linelist: [],
    direction: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 实例化API核心类
    var that = this;
    qqmapsdk = new QQMapWX({
      key: key
    });
    this.setData({
      starttitle: options.starttitle,
      endtitle: options.endtitle,
      startlng: options.startlng,
      startlat: options.startlat,
      endlng: options.endlng,
      endlat: options.endlat,
    });
    qqmapsdk.direction({
      mode: "transit",
      from: options.startlat + "," + options.startlng,
      to: options.endlat + "," + options.endlng,
      success: function (res) {
        that.transline(res);
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  transline(res) {
    var lines = res.result.routes
    this.setData({
      direction: lines
    })
    console.log(lines);
    var linelist = [];
    for (var i = 0; i < lines.length; i++) {
      var line = {};
      line.duration = lines[i].duration;
      var walkdistance = 0;
      var steps = lines[i].steps;
      var buslist = [];
      var busstr = [];
      for (var j = 0; j < steps.length; j++) {
        var step = steps[j];
        if (step.mode === "WALKING") {
          walkdistance = walkdistance + step.distance;
        }
        if (step.mode === "TRANSIT") {
          var busline = step.lines;
          for(var x = 0; x < busline.length; x++) {
            var vehicle = busline[x].vehicle;
            var bus = {
              title: busline[x].title,
              vehicle: busline[x].vehicle
            }
            
            if(vehicle === "BUS") {
                busstr.push({
                  type: 0,
                  line: busline[x].title
                })
              } else if (vehicle === "SUBWAY") {
                busstr.push({
                  type: 1,
                  line: busline[x].title
                })
            }
          }
        }
      }
      line.busstr = busstr;
      line.length = busstr.length - 1;
      line.walkdistance = walkdistance;
      linelist.push(line);
    }
    console.log(linelist);
    this.setData({
      linelist: linelist
    })
  },
  bindViewMap(e) {
    var that = this;
    var index = e.currentTarget.dataset.index
    wx.navigateTo({
      url: "../map/map?index=" + index + "&starttitle=" + that.data.starttitle + "&endtitle=" + that.data.endtitle + "&startlng=" + that.data.startlng + "&startlat=" + that.data.startlat + "&endlng=" + that.data.endlng + "&endlat=" + that.data.endlat + "&flag=roadline"
    });
  }
})