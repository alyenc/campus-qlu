// pages/login/login.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    flag: "",
    cookie: "",
    verify: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      flag: options.flag
    });
    this.verifycode();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindViewTap() {
    this.verifycode();
  },
  verifycode() {
    var that = this;
    wx.request({
      url: 'https://campus.alyenc.com/getVerifyCode.json',
      method: "POST",
      success: function (res) {
        console.log(res);
        that.setData({
          cookie: res.data.value.cookie.JSESSIONID,
          verify: "data:image/jpg;base64," + res.data.value.verifyCode
        })
        wx.hideToast();
      },
      fail: function (err) { },
      complete: function () { }
    })
  },
  invoke_funcs(e) {
    if(this.data.flag == "grade") {
      wx.navigateTo({
        url: '../grade/grade?num=' + e.detail.value.num + "&pass=" + e.detail.value.pass + "&verify=" + e.detail.value.verify + "&session=" + this.data.cookie
      })
    } else if(this.data.flag == "course") {
      var that = this;
      wx.navigateTo({
        url: '../course/course?num=' + e.detail.value.num + "&pass=" + e.detail.value.pass + "&verify=" + e.detail.value.verify + "&session=" + this.data.cookie
      })
    }
  }
})