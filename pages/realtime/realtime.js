// pages/realtime/realtime.js

var ws = require('../../utils/WSCoordinate.js') 

Page({
  /**
   * 页面的初始数据
   */
  data: {
    markers: [{
      id: 0,
      latitude: transLat(116.96727296911, 36.655882388968),
      longitude: transLng(116.96727296911, 36.655882388968),
      width: 10,
      height: 10,
      callout: {
        color: "#2c8df6",
        fontSize: 20,
        borderRadius: 10,
        bgColor: "#fff",
        display: "ALWAYS",
        boxShadow: "2px 2px 10px #aaa"
      }
    }, {
        id: 0,
        latitude: transLat(116.96913085026, 36.660114249612),
        longitude: transLng(116.96913085026, 36.660114249612),
        width: 10,
        height: 10,
        callout: {
          color: "#2c8df6",
          fontSize: 20,
          borderRadius: 10,
          bgColor: "#fff",
          display: "ALWAYS",
          boxShadow: "2px 2px 10px #aaa"
        }
      }],
    polyline: [{
      points: [{
          latitude: transLat(116.96727296911, 36.655882388968),
          longitude: transLng(116.96727296911, 36.655882388968)
        }, {
          latitude: transLat(116.96913085026, 36.660114249612),
          longitude: transLng(116.96913085026, 36.660114249612)
        }, {
          latitude: transLat(116.9727197107, 36.663176966714),
          longitude: transLng(116.9727197107, 36.663176966714)
        }, {
          latitude: transLat(116.97857950369, 36.663551562226),
          longitude: transLng(116.97857950369, 36.663551562226)
        }, {
          latitude: transLat(116.97852854006, 36.666782368183),
          longitude: transLng(116.97852854006, 36.666782368183)
        }, {
          latitude: transLat(116.98370120974, 36.667312820571),
          longitude: transLng(116.98370120974, 36.667312820571)
        }, {
          latitude: transLat(116.98704086778, 36.668216915075),
          longitude: transLng(116.98704086778, 36.668216915075)
      }],
      color: "#ff6600",
      width: 2,
      dottedLine: false,
      arrowLine: true,
      borderColor: "#000",
      borderWidth: 5
    }],
    controls: [{
      id: 1,
      iconPath: '../../images/icon_cate.png',
      position: {
        left: 0,
        top: 300 - 50,
        width: 50,
        height: 50
      },
      clickable: true
    }]
  },
  regionchange(e) {
    console.log(e.type)
  },
  markertap(e) {
    console.log(e.markerId)
  },
  controltap(e) {
    console.log(e.controlId)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})

function transLng(lng, lat) {
  var gcj = ws.transformFromBaiduToGCJ(lat, lng);
  return gcj.longitude; 
}
function transLat(lng, lat) {
  var gcj = ws.transformFromBaiduToGCJ(lat, lng);
  return gcj.latitude;
}