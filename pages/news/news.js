//news.js
//获取应用实例
const app = getApp()

Page({
  data: {
    name: '工大新闻',
    newsList: []
  },
  onLoad: function () {
    var that = this;
    wx.showToast({
      title: '加载中...',
      icon: 'loading',
      duration: 10000
    });
    wx.request({
      url: 'https://campus.alyenc.com/getNewsList.json',
      method: "POST",
      success: function (res) {
        console.log(res.data.value.newsList);
        that.setData({
          newsList: res.data.value.newsList
        })
        wx.hideToast();
      },
      fail: function (err) { },
      complete: function () { }
    })
  },
  funcs_detail: function (event){
    console.log(event);
    wx.navigateTo({
      url: '../newsdetail/newsdetail?url=' + event.currentTarget.dataset.url
    })
  }
})