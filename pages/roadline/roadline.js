// pages/roadline/roadline.js
var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var qqmapsdk;
var key = "PKDBZ-VF63P-SZCDN-LIJOR-2H6WE-BUFBR";

Page({
  /**
   * 页面的初始数据
   */
  data: {
    flag: "",
    starttitle: "",
    endtitle: "",
    startlng: 0,
    startlat: 0,
    endlng: 0,
    endlat: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 实例化API核心类
    qqmapsdk = new QQMapWX({
      key: key
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    if (that.data.flag === "search") {
      var to = that.data.lat + "," + that.data.lng;
      var params = {
        mode: 'transit',
        to: to
      }

      if (that.data.starttitle == "") {
        wx.getLocation({
          success: function (res) {
            console.log(res);
            if(true) {
              that.setData({
                starttitle: "我的位置",
                startlat: res.latitude, 
                startlng: res.longitude
              });
            }
          },
        })
      }
      setTimeout(function () {
        if (that.data.endtitle != "" && that.data.starttitle != "") {
          wx.navigateTo({
            url: "../roadresult/roadresult?starttitle=" + that.data.starttitle + "&endtitle=" + that.data.endtitle + "&startlng=" + that.data.startlng + "&startlat=" + that.data.startlat + "&endlng=" + that.data.endlng + "&endlat=" + that.data.endlat + "&flag=roadline"
          });
          that.setData({
            flag: ""
          });
        }
      }, 500) 
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindViewTap: function (event) {
    if (event.currentTarget.dataset.id === 'start') {
      wx.navigateTo({
        url: '../search/search?id=start' + "&flag=roadline&dir=start"
      })
    }
    if (event.currentTarget.dataset.id === 'end') {
      wx.navigateTo({
        url: '../search/search?id=end' + "&flag=roadline&dir=end"
      })
    }
  }
})