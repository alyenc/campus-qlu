//express.js
//获取应用实例
const app = getApp()

Page({
  data: {
    name: '快递查询',
    com: "",
    num: "",
    show: false,
    companyArray: [],
    exprPath: [],
    exprPathFirst: {},
    exprPathEnd: {}
  },
  onLoad: function (options) {
    var that = this;
    wx.request({
      url: 'https://campus.alyenc.com/getExpressCode.json',
      method: "POST",
      success: function (res) {
        var resValue = res.data.value.expressCode;
        console.log(resValue);
        that.setData({
          companyArray: resValue
        })
      },
      fail: function (err) { },
      complete: function () { }
    })
  },
  bindPickerChange(e) {
    this.setData({
      index: e.detail.value
    })
  },
  invoke_funcs(e) {
    var that = this;
    wx.showToast({
      title: '加载中...',
      icon: 'loading',
      duration: 10000
    });
    wx.request({
      url: 'https://campus.alyenc.com/getExpress.json',
      data: {
        num: e.detail.value.num,
        com: that.data.companyArray[that.data.index].code
      },
      method: "POST",
      success: function (res) {
        var resValue = res.data.value.exprPath;
        var exprPathFirst = resValue[resValue.length - 1];
        var exprPathEnd = resValue[0];
        resValue.splice(0, 1);
        resValue.splice(resValue.length - 2, resValue.length - 1);
        that.setData({
          exprPath: resValue,
          exprPathFirst: exprPathFirst,
          exprPathEnd: exprPathEnd,
          com: that.data.companyArray[that.data.index].com,
          num: e.detail.value.num,
          show: true
        })
        wx.hideToast();
      },
      fail: function (err) { },
      complete: function () { }
    })
  }
})