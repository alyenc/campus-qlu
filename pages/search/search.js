// pages/search/search.js‘

var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var qqmapsdk;
var key = "PKDBZ-VF63P-SZCDN-LIJOR-2H6WE-BUFBR";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    city: "",
    cityCode: "",
    dir: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    // 实例化API核心类
    qqmapsdk = new QQMapWX({
      key: key
    });
    if(options.flag === "roadline") {
      wx.getLocation({
        success: function (res) {
          console.log("获取当前位置：" + res.latitude + "," + res.longitude);
          that.getDistrict(res.latitude, res.longitude)
        },
      })
      that.setData({
        dir: options.dir
      });
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindViewTap() {
    wx.navigateTo({
      url: '../switchcity/switchcity'
    })
  },
  getDistrict(latitude, longitude) {
    var that = this;
    qqmapsdk.reverseGeocoder({
      success: function (res) {
        console.log("获取当前城市：" + res.result.ad_info.city);
        that.setData({
          city: res.result.ad_info.city,
          cityCode: res.result.ad_info.city_code
        });
      },
    })
  },
  getLocationSug(e) {
    var that = this;
    qqmapsdk.search({
      keyword: e.detail.value, 
      region: that.data.city,
      auto_extend: 0,
      success: function (res) {
        that.setData({
          suggests: res.data
        });
      }
    });
  },
  setAddress(e) {
    console.log(e);
    let pages = getCurrentPages();
    let prevPage = pages[pages.length - 2];
    prevPage.setData({
      title: e.currentTarget.dataset.sug.title,
      lng: e.currentTarget.dataset.sug.location.lng,
      lat: e.currentTarget.dataset.sug.location.lat,
      flag: "search"
    });
    if(this.data.dir === "start") {
      prevPage.setData({
        starttitle: e.currentTarget.dataset.sug.title,
        startlng: e.currentTarget.dataset.sug.location.lng,
        startlat: e.currentTarget.dataset.sug.location.lat
      });
    } else if (this.data.dir === "end") {
      prevPage.setData({
        endtitle: e.currentTarget.dataset.sug.title,
        endlng: e.currentTarget.dataset.sug.location.lng,
        endlat: e.currentTarget.dataset.sug.location.lat
      });
    }
    
    wx.navigateBack({
      delta: 1  // 返回上一级页面。
    })
  }
})