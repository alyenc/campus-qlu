// pages/map/map.js
var QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
var qqmapsdk;
var key = "PKDBZ-VF63P-SZCDN-LIJOR-2H6WE-BUFBR";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    index: 0,
    height: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 实例化API核心类
    var that = this;
    this.setData({
      index: options.index
    })
    qqmapsdk = new QQMapWX({
      key: key
    });
    this.setData({
      starttitle: options.starttitle,
      endtitle: options.endtitle
    });
    qqmapsdk.direction({
      mode: "transit",
      from: options.startlat + "," + options.startlng,
      to: options.endlat + "," + options.endlng,
      success: function (res) {
        that.drawMap(res);
      }
    });
    var that = this
    // 获取系统信息
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          height: res.windowHeight
        })
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  drawMap(res) {
    var j = parseInt(this.data.index);
    var _this = this;
    var ret = res.result.routes[j];
    console.log(ret);
    var count = ret.steps.length;
    var pl = [];
    var coors = [];
    //获取各个步骤的polyline
    for (var i = 0; i < count; i++) {
      if (ret.steps[i].mode == 'WALKING' && ret.steps[i].polyline) {
        coors.push(ret.steps[i].polyline);
      }
      if (ret.steps[i].mode == 'TRANSIT' && ret.steps[i].lines[0].polyline) {
        coors.push(ret.steps[i].lines[0].polyline);
      }
    }
    //坐标解压（返回的点串坐标，通过前向差分进行压缩）
    var kr = 1000000;
    for (var i = 0; i < coors.length; i++) {
      for (var j = 2; j < coors[i].length; j++) {
        coors[i][j] = Number(coors[i][j - 2]) + Number(coors[i][j]) / kr;
      }
    }
    //定义新数组，将coors中的数组合并为一个数组
    var coorsArr = [];
    for (var i = 0; i < coors.length; i++) {
      coorsArr = coorsArr.concat(coors[i]);
    }
    //将解压后的坐标放入点串数组pl中
    for (var i = 0; i < coorsArr.length; i += 2) {
      pl.push({ latitude: coorsArr[i], longitude: coorsArr[i + 1] })
    }

    //设置polyline属性，将路线显示出来,将解压坐标第一个数据作为起点
    _this.setData({
      latitude: pl[0].latitude,
      longitude: pl[0].longitude,
      points: pl,
      markers: [{
        iconPath: '../../static/start.png',
        id: 0,
        latitude: pl[0].latitude,
        longitude: pl[0].longitude,
        width: 20,
        height: 20
      }, {
          iconPath: '../../static/end.png',
          id: 1,
          latitude: pl[pl.length - 1].latitude,
          longitude: pl[pl.length - 1].longitude,
          width: 20,
          height: 20
      }],
      polyline: [{
        points: pl,
        color: '#2ab4ff',
        width: 7,
        dottedLine: true,
        arrowLine: true
      }]
    })
  }
})