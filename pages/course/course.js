// pages/course/course.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    listData: [],
    termarray:[
      "2019-2020-2",
      "2019-2020-1",
      "2018-2019-2",
      "2018-2019-1",
      "2017-2018-2",
      "2017-2018-1",
      "2016-2017-2",
      "2016-2017-1",
      "2015-2016-2",
      "2015-2016-1"
    ],
    index: 0,
    selected: "2017-2018-1",
    options: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      options: options
    })
    this.getdata(options);
  },
  bindPickerChange(e) {
    var that = this;
    var index =  e.detail.value;
    this.setData({
      index: index,
      selected: that.data.termarray[index]
    });
    wx.showToast({
      title: '加载中...',
      icon: 'loading',
      duration: 10000
    });
    var that = this;
    wx.request({
      url: 'https://campus.alyenc.com/reloadCourses.json',
      data: {
        cookie: that.data.options.session,
        condition: that.data.termarray[index]
      },
      method: "POST",
      success: function (res) {
        console.log(res.data.value.courses);
        that.setData({
          listData: res.data.value.courses
        });
        wx.hideToast();
      },
      fail: function (err) { },
      complete: function () { }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  getdata(options) {
    var that = this;
    wx.request({
      url: 'https://campus.alyenc.com/getCourse.json',
      data: {
        num: options.num,
        pass: options.pass,
        verify: options.verify,
        cookie: options.session,
        condition: "2017-2018-1"
      },
      method: "POST",
      success: function (res) {
        that.setData({
          listData: res.data.value.courses
        });
        wx.hideToast();
      },
      fail: function (err) { },
      complete: function () { }
    })
  }
})