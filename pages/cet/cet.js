//news.js
//获取应用实例
const app = getApp()

Page({
  data: {
    showView: false,
    grade: "",
    listenGrade: "",
    name: "",
    no: "",
    readGrade: "",
    school: "",
    writeGrade: "",
  },
  invoke_funcs: function (e) {
    var that = this;
    wx.showToast({
      title: '加载中...',
      icon: 'loading',
      duration: 10000
    });
    wx.request({
      url: 'https://campus.alyenc.com/getCetGrade.json',
      data: {
        no: e.detail.value.no,
        name: e.detail.value.name
      },
      method: "POST",
      success: function (res) {
        var resValue = res.data.value;
        that.setData({
          grade: resValue.grade,
          listenGrade: resValue.listenGrade,
          name: resValue.name,
          no: resValue.no,
          readGrade: resValue.readGrade,
          school: resValue.school,
          writeGrade: resValue.writeGrade,
          showView:true
        })
        wx.hideToast();
      },
      fail: function (err) { },
      complete: function () { }
    })
  },
})