//news.js
//获取应用实例
const app = getApp()

Page({
  data: {
    name: '通知公告',
    noticeList: []
  },
  onLoad: function () {
    var that = this;
    wx.showToast({
      title: '加载中...',
      icon: 'loading',
      duration: 10000
    });
    wx.request({
      url: 'https://campus.alyenc.com/getNoticeList.json',
      method: "POST",
      success: function (res) {
        console.log(res.data.value.noticeList);
        that.setData({
          noticeList: res.data.value.noticeList
        })
        wx.hideToast();
      },
      fail: function (err) { },
      complete: function () { }
    })
  },
  funcs_detail: function (event) {
    console.log(event);
    wx.navigateTo({
      url: '../noticedetail/noticedetail?url=' + event.currentTarget.dataset.url
    })
  }
})