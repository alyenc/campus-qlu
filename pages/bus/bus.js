//bus.js
//获取应用实例
//获取应用实例
var app = getApp()
Page({
  data: {
    suggests: []
  },
  //事件处理函数
  getSug: function (e) {
    var that = this;
    wx.showToast({
      title: '加载中...',
      icon: 'loading',
      duration: 10000
    });
    wx.request({
      url: 'https://campus.alyenc.com/getBusList.json',
      method: 'POST',
      data: {
        busline: e.detail.value
      },
      success: function (res) {
        console.log(res);
        if (res.data.code === 0) {
          that.setData({ 
            suggests: res.data.value.busLines
          });
          wx.hideToast();
        }
      }
    });
  },
  onLoad: function () {
    app.editTabBar();
    var that = this;
  }
})
