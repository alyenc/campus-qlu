// pages/busmain/busmain.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabBar: {
      color: "#a9b7b7",
      selectedColor: "#ff8124",
      borderStyle: "white",
      list: [{
        selectedIconPath: "../../static/bus_tab_selected.png",
        iconPath: "../../static/bus_tab.png",
        pagePath: "../bus/bus",
        text: "实时公交",
        clas: "menu-item",
        selected: true,
      }, {
        selectedIconPath: "../../static/line_tab_selected.png",
        iconPath: "../../static/line_tab.png",
        pagePath: "../roadline/roadline",
        text: "路线规划",
        clas: "menu-item",
        selected: false
      }],
      position: "bottom"
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})