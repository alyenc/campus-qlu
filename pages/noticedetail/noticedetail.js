// pages/newsdetail/newsdetail.js

Page({

  /**
   * 页面的初始数据
   */
  data: {
    contentlist: [],
    title: "",
    author: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.showToast({
      title: '加载中...',
      icon: 'loading',
      duration: 10000
    });
    wx.request({
      url: 'https://campus.alyenc.com/getNoticeDetail.json',
      data: {
        url: options.url
      },
      method: "POST",
      success: function (res) {
        console.log(res.data.value);
        var resValue = res.data.value;
        that.setData({
          title: resValue[0],
          author: resValue[1],
          contentlist: resValue
        });
        wx.hideToast();
      },
      fail: function (err) { },
      complete: function () { }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})